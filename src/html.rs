use database;

pub const HEAD: &str = "<head><meta charset=\"utf-8\"/></head>";

pub const REGISTRATION_FORM: &str =
    "<form method=post action=register><h1>Register:</h1><p>username: <input type=text name=username></p><p>password: <input type=password name=password></p><input type=submit value=register></form>";

pub const LOGIN_FORM: &str =
    "<form method=post action=login><h1>Login:</h1><p>username: <input type=text name=username></p><p>password: <input type=password name=password></p><input type=submit value=login></form>";

pub const POST_FORM: &str =
    "<form method=post action=post><input type=text name=content><input type=submit value=post></form>";

pub const LOGOUT_FORM: &str = "<form method=post action=logout><input type=submit value=logout></form>";

pub fn user_view(user: &database::User) -> String {
    div(&format!(
        "<b>{}</b>\
        <form method=post action=follow>\
        <input type=hidden name=user_id value={}>\
        <input type=submit value=follow>\
        </form>",
        user.username,
        user.id
    ))
}

pub fn post_view(post: &database::Post) -> String {
    div(&format!(
        "{}<p>{}</p>",
        user_view(&database::user_from_id(post.user_id).unwrap()),
        post.content
    ))
}

pub fn div(content: &str) -> String {
    format!("<div><br/>{}</div>", content)
}

pub fn page(content: &str) -> String {
    format!("<!DOCTYPE html><html>{}<body>{}</body></html>", HEAD, content)
}

pub fn logged_in_page(user_id: i32) -> String {
    let mut page = format!("{}{}", POST_FORM, LOGOUT_FORM);

    let mut following_article = String::new();
    for following in database::following(user_id) {
        following_article.push_str(&user_view(&following));
    }
    page.push_str(&div(&following_article));

    let mut posts_article = String::new();
    for post in database::posts() {
        posts_article.push_str(&post_view(&post));
    }
    page.push_str(&div(&posts_article));
    page
}
